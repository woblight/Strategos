Fields explanation

```lua
    ["original string (do not edit)"] = {
        ["disambiguation (do not edit)"] = { "translated string 1 (replace it!)" },
        ["disambiguation (do not edit)"] = { "translated string 2 (replace it!)" },
        ["disambiguation (do not edit)"] = { "translated string 3 (replace it!)" },
    },
```

Some string may contains spacial characters and might be difficoult, for any trouble contact me on discord.
- `^`: beginning of the string
- `.-`: any number of any character
- `.+`: one or more of any character
- `.*`: 0 or more of any character
- `%s+`: one or more spaces
- `( )`: this is required to be present in the translated string, it "captures" a part of it.
        For instance `^(.*) is under attack!%s+If left` captures everything from the beginning
        of the string till ` is`.
