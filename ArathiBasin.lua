local addonName, addonTable = ...

addonTable.Vector2DMixin = Vector2DMixin

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

EFrame.newClass("ArathiBasin", Battleground)

ArathiBasin.id = 529
idLookups[ArathiBasin.id] = ArathiBasin

ARATHI_BASIN_ZONENAME = tr("Arathi Basin", "Zone name as reported by `/run message(GetSubZoneText())` at arathi entrance (in battleground)")

local ArathiBases = {
    [tr("Stables")] = {
        x = .25,
        y = .25,
        type = Node.Stables
    },
    [tr("Gold Mine")] = {
        x = .75,
        y = .25,
        type = Node.GoldMine
    },
    [tr("Blacksmith")] = {
        x = .5,
        y = .5,
        type = Node.Blacksmith
    },
    [tr("Lumber Mill")] = {
        x = .25,
        y = .75,
        type = Node.LumberMill
    },
    [tr("Farm")]       = {
        x = .75,
        y = .75,
        type = Node.Farm
    },
}
local ArathiBasesChatLookup = {
    [tr("the stables")] = tr("Stables"),
    [tr("the mine")] = tr("Gold Mine"),
    [tr("the blacksmith")] = tr("Blacksmith"),
    [tr("the lumber mill")] = tr("Lumber Mill"),
    [tr("the farm")] = tr("Farm"),
    [tr("the southern farm")] = tr("Farm"),
}

ArathiBasin.AllianceAhead = Alliance
ArathiBasin.HordeAhead = Horde
ArathiBasin.Stalling = 0

ArathiBasin:attach("allianceTimeToWin")
ArathiBasin:attach("hordeTimeToWin")
ArathiBasin:attach("winningTeam")
ArathiBasin.__allianceTimeToWin = 0
ArathiBasin.__hordeTimeToWin = 0
ArathiBasin.__winningTeam = ArathiBasin.Stalling


AB_BAR_COLORS = {
    [ArathiBasin.Stalling] = {1,1,1},
    [ArathiBasin.AllianceAhead] = {0,0,1},
    [ArathiBasin.HordeAhead] = {1,0,0}
}

AB_BAR_BG_COLORS = {
    [ArathiBasin.Stalling] = {1,1,1,.25},
    [ArathiBasin.AllianceAhead] = {1,0,0,.25},
    [ArathiBasin.HordeAhead] = {0,0,1,.25}
}

local EFrame = EFrame.Blizzlike
function ArathiBasin:new()
    Battleground.new(self)
    self.window = EFrame.Window()
    self.window.title = tr("Arathi Basin", "Window title")
    self.window.minimumHeight = 1
    self.window.centralItem = EFrame.Item(self.window)
    self.window.centralItem.implicitHeight = 195
    for k, v in pairs(ArathiBases) do
        local n = Node(k, v.type, self)
        local i = EFrame.Image(self.window.centralItem)
        i.anchorCenter = self.window.topLeft
        i.hoffset = EFrame.bind(function() return v.x * self.window.width end)
        i.voffset = EFrame.bind(function() return -v.y * self.window.height end)
        i.implicitWidth = EFrame.bind(function() return self.window.width * 0.15 end)
        i.implicitHeight = EFrame.bind(function() return self.window.height * 0.15 end)
        i.source = "Interface\\Minimap\\POIIcons.blp"
        function i:updateTexture(d)
            x, y = d%8, math.floor(d/8)
            i:setCoords(x/8,(x+1)/8,y/8,(y+1)/8)
        end
        n:connect("textureIndexChanged", i, "updateTexture")
        i:updateTexture(n.textureIndex)
        i.z=4
        i.opacity = EFrame.bind(function() return bit.band(n.status, Node.Contested) ~= 0 and 1 or .75 end)
        local t = SpinningCountdown(self.window.centralItem)
        t.implicitWidth = EFrame.bind(function() return i.width * 2 end)
        t.implicitHeight = EFrame.bind(function() return i.height * 2 end)
        t.anchorCenter = i.center
        t.message = n.name
        t.opacity = 0
        
        n:connect("assaulted", function(time) t:restart(time, 60) end)
        n:connect("statusChanged", function (s)
            t:stop()
        end)
        self.nodes[k] = n
    end
    self.nodes[ARATHI_BASIN_ZONENAME] = Node(ARATHI_BASIN_ZONENAME, Node.Graveyard, self)
    self.nodes[ARATHI_BASIN_ZONENAME].status = playerFaction
    self.window.centralItem.model = EFrame.ListModel(self)
    if StrategosProfile.arathiWindowHeight then
        self.window.height = EFrame.normalizeBind(StrategosProfile.arathiWindowHeight)
    end
    if StrategosProfile.arathiWindowWidth then
        self.window.width = EFrame.normalizeBind(StrategosProfile.arathiWindowWidth)
    end
    if StrategosProfile.arathiWindowX then
        self.window.marginLeft = EFrame.normalizeBind(StrategosProfile.arathiWindowX)
    end
    if StrategosProfile.arathiWindowY then
        self.window.marginTop = EFrame.normalizeBind(StrategosProfile.arathiWindowY)
    end
    self.window:connect("marginTopChanged", function(v) StrategosProfile.arathiWindowY = EFrame.normalized(v) end)
    self.window:connect("marginLeftChanged", function(v) StrategosProfile.arathiWindowX = EFrame.normalized(v) end)
    self.window:connect("heightChanged", function(v) if self.window.implicitHeight ~= v then StrategosProfile.arathiWindowHeight = EFrame.normalized(v) end end)
    self.window:connect("widthChanged", function(v) if self.window.implicitWidth ~= v then StrategosProfile.arathiWindowWidth = EFrame.normalized(v) end end)
    self.ttw = EFrame.Rectangle()
    local anchorFrame = UIWidgetTopCenterContainerFrame
    self.ttw.anchorTop = {frame = anchorFrame, point = "BOTTOM"}
    self.ttw.width = 250
    self.ttw.height = 20
    self.ttw.bar = EFrame.ProgressBar(self.ttw)
    self.ttw.bar.texture = "Interface\\TargetingFrame\\UI-StatusBar"
    self.ttw.bar.anchorFill = self.ttw
    self.ttw.color = EFrame.bind(function() return AB_BAR_BG_COLORS[self.winningTeam] end)
    self.ttw.bar.color = EFrame.bind(function() return AB_BAR_COLORS[self.winningTeam] end)
    self.ttw.text = EFrame.Label(self.ttw)
    self.ttw.text.text = EFrame.bind(function ()
        if self.winningTeam == Alliance then
            return self.allianceTimeToWin > 0 and format(tr("Alliance will win in: %02d:%02d"), math.floor(self.allianceTimeToWin/60), self.allianceTimeToWin % 60) or tr("Alliance won!")
        elseif self.winningTeam == Horde then
            return self.hordeTimeToWin > 0 and format(tr("Horde will win in: %02d:%02d"), math.floor(self.hordeTimeToWin/60), self.hordeTimeToWin % 60) or tr("Horde won!")
        else
            return tr("Stalling")
        end
    end)
    self.ttw.bar.from = 0
    self.ttw.bar.to = 10000
    EFrame.root:connect("update", self.ttw, "update")
    function self.ttw.update()
        local a, h = self.team[Alliance].winTime, self.team[Horde].winTime
        if not a and not h then
            self.winningTeam =  ArathiBasin.Stalling
            self.ttw.bar.value = 0
            return
        end
        if a then
            a = self.team[Alliance].winTime - GetTime()
            self.allianceTimeToWin = a
        end
        if h then
            h = self.team[Horde].winTime - GetTime()
            self.hordeTimeToWin = h
        end
        if not a then
            self.winningTeam = ArathiBasin.HordeAhead
            self.ttw.bar.value = 10000
        elseif not h then
            self.winningTeam = ArathiBasin.AllianceAhead
            self.ttw.bar.value = 10000
        else
            self.winningTeam = self.allianceTimeToWin < self.hordeTimeToWin and ArathiBasin.AllianceAhead or self.allianceTimeToWin > self.hordeTimeToWin and ArathiBasin.HordeAhead or ArathiBasin.Stalling
            if a < 0 or h < 0 then
                self.ttw.bar.value = 10000
            else
                self.ttw.bar.value = (1 - math.min(a, h)/math.max(h,a))*10000
            end
        end
    end
    
    self.ttw.text.anchorCenter = self.ttw
    self.b = SpiritHealerIndicator()
    self.b.anchorTop = self.ttw.bottom
    self:updateWorldStates()
    self.team = {[Alliance] = {}, [Horde] = {}}
    self.startTimer:clearAnchors()
    self.startTimer.marginTop = 14
    self.startTimer.anchorTop = self.b.bottom
end

function ArathiBasin:destroy()
    self.b:destroy()
    self.ttw:destroy()
    self.window:destroy()
    Battleground.destroy(self)
end

local evs = {
    [tr("1 minute.*Arathi Basin")] = function (self)
        self.status = Battleground.Starting
        self:starting(60)
    end,
    [tr("30 seconds.*Arathi Basin")] = function (self)
        self.status = Battleground.Starting
        self:starting(30)
    end,
    [tr("Arathi Basin.* has begun")] = function (self)
        self.status = nil
        self:started()
    end,
    [tr("claims (.-)!")] = function (self, message, faction, node)
        self.nodes[ArathiBasesChatLookup[node]]:assault(60, faction)
    end,
    [tr("assaulted (.-)!")] = function (self, message, faction, node)
        self.nodes[ArathiBasesChatLookup[node]]:assault(60, faction)
    end,
    [tr("taken (.-)!")] = function (self, message, faction, node)
        self.nodes[ArathiBasesChatLookup[node]]:capture(faction)
    end,
    [tr("defended (.-)!")] = function (self, message, faction, node)
        self.nodes[ArathiBasesChatLookup[node]]:capture(faction)
    end,
}

function ArathiBasin:processChatEvent(message, faction)
    for s,f in pairs(evs) do
        local r = {strfind(message, s)}
        if #r > 0 then
            f(self, message, faction, select(3,unpack(r)))
            return
        end
    end
    Battleground.processChatEvent(self, message, faction)
end

local scoreKitFaction = {[5248] = Alliance, [5249] = Horde}

function ArathiBasin:updateWorldStates(payload)
    if not self.b.ok then
        self.b:restart(self.nodes[ARATHI_BASIN_ZONENAME])
        self.b.ok = true
    end
    local a = C_Map.GetBestMapForUnit("player")
    if a then
        for _,v in pairs(C_AreaPoiInfo.GetAreaPOIForMap(a)) do
            local i = C_AreaPoiInfo.GetAreaPOIInfo(a, v)
            if self.nodes[i.name] then
                self.nodes[i.name].status = POIInfo[i.textureIndex].status
            end
        end
    end
    
    if payload then
        local now = GetTime()
        local info = C_UIWidgetManager.GetIconAndTextWidgetVisualizationInfo(payload.widgetID)
        local msg = info.text
        local i = scoreKitFaction[info.textureKitID]
        if not msg then
            return
        end
        local _,_, n, s = strfind(msg, "(%d).-%D(%d+)/")
        if not n then return end
        n = tonumber(n)
        s = tonumber(s)
        if self.team[i].lastN ~= n or self.team[i].lastScore ~= s then
            if self.team[i].lastScore ~= s then
                self.team[i].lastScore = s
                self.team[i].lastTime = now
            end
            if n == 0 then
                self.team[i].winTime = nil
            else
                local tt = n == 5 and 1 or (5 - n) * 3
                local pp = n == 5 and 30 or 10
                local w = math.ceil((2000 - s) / pp) * tt
                self.team[i].winTime = self.team[i].lastTime + w
            end
        end
    end
end

local grave = setmetatable({
}, { __index = function(_,k) return k end })

local lastSpiritHealerTime = 0
function ArathiBasin:update()
    local node = self.nodes[grave[GetSubZoneText()]] or self.nodes[ARATHI_BASIN_ZONENAME]
    local time = GetAreaSpiritHealerTime()
    if lastSpiritHealerTime and lastSpiritHealerTime ~= time and time > 1 and time < 30 then
        if not node then return end
        lastSpiritHealerTime = time
        local off = node:spiritHealerTime() - time -1
        if math.abs(off) > .25 then
            node.ressoff = node.ressoff + off
        end
    elseif time ~= 0 then
        lastSpiritHealerTime = time
    else
        lastSpiritHealerTime = nil
    end
    if node and self.b.node ~= node and node.status == playerFaction and (GetAreaSpiritHealerTime() > 0 or node.name ~= ARATHI_BASIN_ZONENAME) then
        self.b:restart(node)
    end
end
