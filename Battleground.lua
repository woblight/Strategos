local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

resstime, resttime = 30, 1.6
tresstime = resstime + resttime

Alliance = 1
Horde = 2

EFrame.newClass("Carrier", EFrame.Object)

Carrier:attach("name")
Carrier:attach("health")
Carrier:attach("class")
Carrier:attach("faction")
Carrier:attachSignal("hittedClose")
Carrier:attachSignal("hittedFar")

EFrame.newClass("Flag", EFrame.Object)

Flag.Carried = 1
Flag.Ground = 2
Flag.Resetting = 3

Flag:attach("status")
Flag:attach("carrier")
Flag:attachSignal("picked")
Flag:attachSignal("resetting")
Flag:attachSignal("dropped")
Flag:attachSignal("placed")

function Flag:pick(name)
    if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
        name = name ~= "" and name
        self.status = Flag.Carried
        self.carrier.name = name
        local class = self.parent:playerClass(name)
        self.carrier.class = class
        if not class then
            RequestBattlefieldScoreData()
        end
    else
        self.status = Flag.Carried
        self.carrier.name = UnitName(name)
        _, self.carrier.class = UnitClass(name)
    end
    self:picked()
end

function Flag:drop()
    self.status = Flag.Ground
    self.carrier.name = nil
    self:resetting(10)
end

function Flag:reset()
    self.status = Flag.Resetting
    self.carrier.name = nil
    self:resetting(10)
end

function Flag:place()
    self.status = nil
    self.carrier.name = nil
    self:placed()
end

EFrame.newClass("Node", EFrame.Object)

Node.Neutral = 0
Node.Alliance = Alliance
Node.Horde = Horde
Node.Contested = 4
Node.Mine = 1
Node.Graveyard = 2
Node.Tower = 3
Node.Stables = bit.lshift(1,2)
Node.GoldMine = bit.lshift(2,2)
Node.Blacksmith = bit.lshift(3,2)
Node.LumberMill = bit.lshift(4,2)
Node.Farm = bit.lshift(5,2)
Node.AllianceContested = bit.bor(Node.Contested, Node.Alliance)
Node.HordeContested = bit.bor(Node.Contested, Node.Horde)
Node.FactionMask = bit.bor(Node.Alliance, Node.Horde)
Node.TypeMask = 3

Node:attach("status")
Node:attach("name")
Node:attach("textureIndex")
Node:attach("ressoff")
Node:attachSignal("assaulted")
Node:attachSignal("captured")
Node.__ressoff = 0

function Node:new(name, type, parent)
    EFrame.Object.new(self, parent)
    self.name = name
    self.type = type
    self:connect("statusChanged", function (status)
        self.textureIndex = lookupPOIIndex(self.type, status)
    end)
    self.status = Node.Neutral
end

function Node:assault(t, faction)
    self.status = bit.bor(Node.Contested, faction)
    self:assaulted(t)
end

function Node:capture(faction)
    self.status = faction
    self:captured()
    if bit.band(self.type, Node.TypeMask) == Node.Graveyard and faction == playerFaction then
        self.ressoff = self:spiritHealerTime() - 2.5
    end
end

function Node:spiritHealerTime()
    return tresstime - (GetTime() - self.parent.bgtime - 132.5 + self.ressoff)%tresstime
end

EFrame.newClass("Battleground", EFrame.Object)

POIInfo = {
    [0] = {type = Node.Mine, status = Node.Neutral},
    [1] = {type = Node.Mine, status = Node.Horde},
    [2] = {type = Node.Mine, status = Node.Alliance},
    [3] = {type = Node.Graveyard, status = Node.AllianceContested},
    [5] = {type = Node.Tower, status = Node.Neutral},
    [7] = {type = Node.Graveyard, status = Node.Neutral},
    [8] = {type = Node.Tower, status = Node.AllianceContested},
    [9] = {type = Node.Tower, status = Node.Horde},
    [10] = {type = Node.Tower, status = Node.Alliance},
    [11] = {type = Node.Tower, status = Node.HordeContested},
    [12] = {type = Node.Graveyard, status = Node.Horde},
    [13] = {type = Node.Graveyard, status = Node.HordeContested},
    [14] = {type = Node.Graveyard, status = Node.Alliance},
    [16] = {type = Node.GoldMine, status = Node.Neutral},
    [17] = {type = Node.GoldMine, status = Node.AllianceContested},
    [18] = {type = Node.GoldMine, status = Node.Alliance},
    [19] = {type = Node.GoldMine, status = Node.HordeContested},
    [20] = {type = Node.GoldMine, status = Node.Horde},
    [21] = {type = Node.LumberMill, status = Node.Neutral},
    [22] = {type = Node.LumberMill, status = Node.AllianceContested},
    [23] = {type = Node.LumberMill, status = Node.Alliance},
    [24] = {type = Node.LumberMill, status = Node.HordeContested},
    [25] = {type = Node.LumberMill, status = Node.Horde},
    [26] = {type = Node.Blacksmith, status = Node.Neutral},
    [27] = {type = Node.Blacksmith, status = Node.AllianceContested},
    [28] = {type = Node.Blacksmith, status = Node.Alliance},
    [29] = {type = Node.Blacksmith, status = Node.HordeContested},
    [30] = {type = Node.Blacksmith, status = Node.Horde},
    [31] = {type = Node.Farm, status = Node.Neutral},
    [32] = {type = Node.Farm, status = Node.AllianceContested},
    [33] = {type = Node.Farm, status = Node.Alliance},
    [34] = {type = Node.Farm, status = Node.HordeContested},
    [35] = {type = Node.Farm, status = Node.Horde},
    [36] = {type = Node.Stables, status = Node.Neutral},
    [37] = {type = Node.Stables, status = Node.AllianceContested},
    [38] = {type = Node.Stables, status = Node.Alliance},
    [39] = {type = Node.Stables, status = Node.HordeContested},
    [40] = {type = Node.Stables, status = Node.Horde},
}

function lookupPOIIndex(type, status)
    for k, v in pairs(POIInfo) do
        if v.type == type and status == v.status then
            return k
        end
    end
    return -1
end

Battleground.Starting = 1
Battleground.Finished = 2

Battleground:attach("status")
Battleground:attachSignal("starting")
Battleground:attachSignal("started")
Battleground:attachSignal("finished")

function Battleground:new()
    EFrame.Object.new(self)
    self.startTimer = SpinningCountdown()
    self.startTimer.marginRight = 20 * UIParent:GetScale() / EFrame.root.scale
    self.startTimer.anchorRight = {frame = UIWidgetTopCenterContainerFrame, point = "LEFT"}
    self.startTimer.width = UIWidgetTopCenterContainerFrame:GetHeight() * 0.66
    self.startTimer.height = UIWidgetTopCenterContainerFrame:GetHeight() * 0.66
    self.startTimer.message = "Starting"
    self.startTimer.showTime = true
    self:connect("starting", function(v) self.startTimer:restart(v,120) end)
    self.startTimer.visible = EFrame.bind(function() return self.status == Battleground.Starting end)
    self.nodes = {}
    self.bgtime = 0
--     if StrategosMinimap then
--         StrategosBattlegroundMinimapStarting:setTimer(o.startTimer)
--         StrategosMinimapPlugin.frames = { StrategosBattlegroundMinimapStarting }
--     end
end

function Battleground:destroy()
    self.startTimer:destroy()
    EFrame.Object.destroy(self)
end

function Battleground:update()
end

-- local ArathiBases = {
--     ST = {
--         x = 0.3747319206595421,
--         y = 0.2917306870222092
--     },
--     GM = {
--         x = 0.5751497000455856,
--         y = 0.3086424916982651
--     },
--     BS = {
--         x = 0.4622423946857452,
--         y = 0.4537641629576683
--     },
--     LM = {
--         x = 0.4039658159017563,
--         y = 0.5570576936006546
--     },
--     FM = {
--         x = 0.5603127181529999,
--         y = 0.5996280610561371
--     }
-- }
-- 
-- 
-- AlteracPOILookup = {}
--[[
function StrategosMinimapPlugin:load()
    CreateFrame("Frame","StrategosBattlegroundMinimapStarting",StrategosMinimap,"StrategosRingFrameTemplate")
    StrategosMinimapPOI:new(StrategosBattlegroundMinimapStarting)
    StrategosBattlegroundMinimapStarting:SetPoint("CENTER",StrategosMinimap)
    StrategosBattlegroundMinimapStarting.buildTooltip = function(self)
        local left = self.timer:remaning()
        local m,s = floor(left/60), mod(left, 60)
        return format(tr("BG_START_IN_UI","client")..": %d:%02d", m, s)
    end
    StrategosBattlegroundMinimapStarting.buildMenu = function(self)
        if self.timer:remaning() then
            UIDropDownMenu_AddButton({
                text = tr("BG_START_IN_UI","client").."...",
                func = function()
                    local left = self.timer:remaning()
                    if left then
                        local m,s = floor(left/60), mod(left, 60)
                        SendChatMessage(tr("BG_START_IN_CHAT","chat",{time = format("%d:%02d", m, s)}), "BATTLEGROUND")
                    end
                end
            })
        end
    end
    
    for n,b in pairs(ArathiBases) do
        local f = getglobal("StrategosMinimapArathiIndicator"..n)
        if not f then
            f = CreateFrame("Frame", "StrategosMinimapArathiIndicator"..n, StrategosMinimap, "StrategosIndicatorFrameTemplate")
            StrategosMinimapPOI:new(f)
            f:SetPoint("CENTER", StrategosMinimap, "TOPLEFT", b.x * StrategosMinimap:GetWidth(), -b.y * StrategosMinimap:GetHeight())
            f.buildTooltip = function(self, t)
                local left = self.node.timer:remaning()
                if left then
                    local m,s = floor(left/60), mod(left, 60)
                    return format(self.node.name..": %d:%02d", m, s)
                end
            end
            f.buildMenu = function(self)
                if self.node.timer:remaning() then
                    UIDropDownMenu_AddButton({
                        text = "[BG] " .. self.node.name,
                        func = function()
                            local left = self.node.timer:remaning()
                            if left then
                                local m,s = floor(left/60), mod(left, 60)
                                SendChatMessage(format(self.node.name..": %d:%02d", m, s), "BATTLEGROUND")
                            end
                        end
                    })
                end
            end
        end
    end
    
    for n,b in pairs(AlteracBases) do
        local f = getglobal("StrategosMinimapAlteracIndicator"..n)
        if not f then
            f = CreateFrame("Frame", "StrategosMinimapAlteracIndicator"..n, StrategosMinimap, "StrategosIndicatorFrameTemplate")
            f:SetPoint("CENTER", StrategosMinimap, "TOPLEFT", b.x * StrategosMinimap:GetWidth(), -b.y * StrategosMinimap:GetHeight())
            AlteracPOILookup[b.name] = f
            StrategosMinimapPOI:new(f)
            f.name = b.name
            f.buildTooltip = function(self, t)
                local left = self.node.timer:remaning()
                if left then
                    local m,s = floor(left/60), mod(left, 60)
                    return format(self.node.name..": %d:%02d", m, s)
                end
            end
            f.buildMenu = function(self)
                if self.node.timer:remaning() then
                    UIDropDownMenu_AddButton({
                        text = "[BG] " .. self.node.name,
                        func = function()
                            local left = self.node.timer:remaning()
                            if left then
                                local m,s = floor(left/60), mod(left, 60)
                                SendChatMessage(format(self.node.name..": %d:%02d", m, s), "BATTLEGROUND")
                            end
                        end
                    })
                end
            end
            f.pin:SetTexture("Interface\\Minimap\\POIIcons")
            f.colorful = true
            f:Hide()
        end
    end
    
    local f = getglobal("StrategosMinimapEFCIndicator")
    if not f then
        f = CreateFrame("Model", "StrategosMinimapEFCIndicator", StrategosMinimap)
        f:SetWidth(125)
        f:SetHeight(125)
        f:SetPosition(-0.0125,-0.0125,0)
        f:SetModel("Interface\\MiniMap\\Ping\\MinimapPing.mdx")
        f.closeTimer = Timer:new(2.5)
        f.farTimer = Timer:new(2.5)
        f:SetScript("OnUpdate",function()
            if f.closeTimer:remaning() then
                self:ClearAllPoints()
                self:SetPoint("CENTER", getglobal("StrategosMinimapDot"..self.closeIndex))
                self:SetScale(0.4)
                self:SetAlpha(1)
                self:Show()
            elseif f.farTimer:remaning() then
                self:ClearAllPoints()
                self:SetPoint("CENTER", getglobal("StrategosMinimapDot"..self.farIndex))
                self:SetScale(1)
                self:SetAlpha(0.33)
                self:Show()
            else
                self:Hide()
            end
        end)
        Object.connect(f.closeTimer, "started", f, f.Show)
        Object.connect(f.farTimer, "started", f, f.Show)
    end
end]]

function Battleground:updateScore()
    if self.bgtime == 0 then
        local time = GetBattlefieldInstanceRunTime()/1000
        if time ~= 0 then
            self.bgtime = GetTime() - time
            if time < 130 and not self.status then
                self.startTimer:restart(120 - time + 10, 120)
                self.status = Battleground.Starting
            end
        end
    end
end

function Battleground:updateWorldStates()
end

local playerClassLock
function Battleground:playerClass(name)
    if playerClassLock then return end
    playerClassLock = true
    SetBattlefieldScoreFaction(-1)
    for i = 1, GetNumBattlefieldScores() do
        sn, _, _, _, _, _, _, _, sc = GetBattlefieldScore(i)
        if sn == name then
            playerClassLock = nil
            return strupper(sc)
        end
    end
    playerClassLock = nil
end

function Battleground:processChatEvent(message, faction)
    if StrategosSettings.debug then
        print(format("Unhandled chat \"%s\" faction %d", message, faction))
    end
end

-- if not StrategosMinimap_Plugins then
--     getfenv(0).StrategosMinimap_Plugins = {}
-- end

-- function Battleground:processAddOnMessage()
-- end

if C_CreatureInfo.GetFactionInfo(select(3, UnitRace("player"))).name == "Horde" then
    playerFaction = Horde
else
    playerFaction = Alliance
end
