local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

Localizations = {}

local l = Localizations[GetLocale()] or {}

function tr(string, disambiguation, n)
    local s = l[string]
    if not s then
        return string
    end
    if not disambiguation then
        return s[1] or string
    elseif not n then
        return s[disambiguation] or string
    end
    for i = n,1,-1 do
        if s[disambiguation][i] then
            return format(s[disambiguation][i], n)
        end
    end
end

Localizations = {
}
