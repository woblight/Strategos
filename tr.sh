#!/bin/bash

file="Localization_$1.lua"
strings="$(gawk -v old=$file -v lang=$1 -f tr.awk *.lua)"

cat >$file << EndOfMessage
local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

Localizations.$1 = {
$strings
}
EndOfMessage


