local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

Localizations.deDE = {
    ["Arathi Basin.* has begun"] = {
        [1] = { "Arathi Basin.* has begun" },
    },
    ["30 seconds.*Arathi Basin"] = {
        [1] = { "30 seconds.*Arathi Basin" },
    },
    ["__GROUND__"] = {
        [1] = { "__GROUND__" },
    },
    ["1 minute.*Arathi Basin"] = {
        [1] = { "1 minute.*Arathi Basin" },
    },
    ["Frostwolf Keep"] = {
        [1] = { "Burg Frostwolf" },
    },
    ["captured"] = {
        [1] = { "erobert" },
    },
    ["the stables"] = {
        [1] = { "the stables" },
    },
    ["Stalling"] = {
        [1] = { "Stalling" },
    },
    ["the lumber mill"] = {
        [1] = { "the lumber mill" },
    },
    ["Farm"] = {
        [1] = { "Farm" },
    },
    ["Stonehearth Graveyard"] = {
        [1] = { "Stonehearth-Friedhof" },
    },
    ["Dun Baldar"] = {
        [1] = { "Dun Baldar" },
    },
    ["Blacksmith"] = {
        [1] = { "Blacksmith" },
    },
    ["Frostwolf Graveyard"] = {
        [1] = { "Friedhof der Frostwolf" },
    },
    ["Lumber Mill"] = {
        [1] = { "Lumber Mill" },
    },
    ["Alliance won!"] = {
        [1] = { "Alliance won!" },
    },
    ["assaulted (.-)!"] = {
        [1] = { "assaulted (.-)!" },
    },
    ["East Frostwolf Tower"] = {
        [1] = { "Östlicher Frostwolfturm" },
    },
    ["Horde"] = {
        [1] = { "Horde" },
    },
    ["Horde will win in: %02d:%02d"] = {
        [1] = { "Horde will win in: %02d:%02d" },
    },
    ["^the "] = {
        ["remove article in \"claims (.*) graveyard\""] = { "^den " },
    },
    ["defended (.-)!"] = {
        [1] = { "defended (.-)!" },
    },
    ["the southern farm"] = {
        [1] = { "the southern farm" },
    },
    ["^The "] = {
        ["remove article in \"^(.*) is under attack!%s+If left\""] = { "^Der " },
        ["remove article in \"^(.*) was taken by\""] = { "^der " },
        ["\"^(.*) was destroyed by\""] = { "^der " },
    },
    ["Horde Flag"] = {
        [1] = { "Flagge der Horde" },
    },
    ["30 seconds.*Alterac Valley"] = {
        [1] = { "30 Sekunden.*Alteractal" },
    },
    ["Stonehearth Bunker"] = {
        [1] = { "Stonehearth-Bunker" },
    },
    ["Icewing Bunker"] = {
        [1] = { "Icewing-Bunker" },
    },
    ["Coldtooth Mine"] = {
        [1] = { "Coldtooth-Mine" },
    },
    ["Warsong Gulch.- 1 minute"] = {
        [1] = { "Warsongschlucht.- 1 Minute" },
    },
    ["claims (.*) graveyard"] = {
        [1] = { "^(.*) wurde von.*erobert`" },
    },
    ["Frostwolf Relief Hut"] = {
        [1] = { "Hütte der Heiler von Frostwolf" },
    },
    ["Alliance Flag"] = {
        [1] = { "Flagge der Allianz" },
    },
    ["^(.*) is under attack!%s+If left"] = {
        [1] = { "^(.*) wird angegriffen!%s+Wenn er unbewacht bleibt" },
    },
    ["by (.+)!"] = {
        [1] = { "von (.+)!" },
    },
    ["Dun Baldar South Bunker"] = {
        [1] = { "Südbunker von Dun Baldar" },
    },
    ["Alterac Valley.* has begun"] = {
        [1] = { "Alteractal.* hat begonnen" },
    },
    ["the mine"] = {
        [1] = { "the mine" },
    },
    ["Tower Point"] = {
        [1] = { "Turmstellung" },
    },
    ["dropped"] = {
        [1] = { "fallen lassen" },
    },
    ["^(.*) was taken by"] = {
        [1] = { "^(.*) wurde von.*erobert`" },
    },
    ["returned"] = {
        [1] = { "zurückgebracht" },
    },
    ["Alliance will win in: %02d:%02d"] = {
        [1] = { "Alliance will win in: %02d:%02d" },
    },
    ["1 minute.*Alterac Valley"] = {
        [1] = { "1 Minute.*Alteractal" },
    },
    ["the blacksmith"] = {
        [1] = { "the blacksmith" },
    },
    ["claims (.-)!"] = {
        [1] = { "claims (.-)!" },
    },
    ["Arathi Basin"] = {
        ["Zone name as reported by `/run message(GetSubZoneText())` at arathi entrance (in battleground)"] = { "Arathi Basin" },
        ["Window title"] = { "Arathi Basin" },
    },
    ["the farm"] = {
        [1] = { "the farm" },
    },
    ["Snowfall Graveyard"] = {
        [1] = { "Snowfall-Friedhof" },
    },
    ["Alliance"] = {
        [1] = { "Allianz" },
    },
    ["^(.*) was destroyed by"] = {
        [1] = { "^(.*) wurde von.*zerstört`" },
    },
    ["picked"] = {
        [1] = { "aufgenommen" },
    },
    ["Alterac Valley"] = {
        ["Zone name as reported by `/run message(GetSubZoneText())` at alterac entrance (in battleground)"] = { "Alteractal" },
        ["Window title"] = { "Alteractal" },
    },
    ["Dun Baldar North Bunker"] = {
        [1] = { "Nordbunker von Dun Baldar" },
    },
    ["[aA]lliance"] = {
        [1] = { "[aA]lliance" },
    },
    ["Stables"] = {
        [1] = { "Stables" },
    },
    ["[Hh]orde"] = {
        [1] = { "[Hh]orde" },
    },
    ["taken (.-)!"] = {
        [1] = { "taken (.-)!" },
    },
    ["Iceblood Graveyard"] = {
        [1] = { "Iceblood-Friedhof" },
    },
    ["^Let the battle for Warsong Gulch begin!"] = {
        [1] = { "^Lasst den Kampf um die Warsongschlucht beginnen!" },
    },
    ["Irondeep Mine"] = {
        [1] = { "Irondeep-Mine" },
    },
    ["placed"] = {
        [1] = { "aufgestellt" },
    },
    ["Gold Mine"] = {
        [1] = { "Gold Mine" },
    },
    ["Stormpike Graveyard"] = {
        [1] = { "Stormpike-Friedhof" },
    },
    ["Stormpike Aid Station"] = {
        [1] = { "Stormpike-Lazarett" },
    },
    ["Horde won!"] = {
        [1] = { "Horde won!" },
    },
    ["Iceblood Tower"] = {
        [1] = { "Iceblood-Turm" },
    },
    ["Warsong Gulch.- 30 seconds"] = {
        [1] = { "Warsongschlucht.- 30 Sekunden" },
    },
    ["West Frostwolf Tower"] = {
        [1] = { "Westlicher Frostwolfturm" },
    },
}
